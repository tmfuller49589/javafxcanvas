package hellojfx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

public class Main extends Application {
	private Stage primaryStage;
	private GraphicsContext graphicsContext;
	private Canvas canvas;

	public Main() {

	}

	@Override
	public void start(Stage primaryStage) {
		StackPane root = new StackPane();

		int width = 500;
		int height = 500;
		Scene scene = new Scene(root, width, height);

		primaryStage.setTitle("JavaFX canvas");
		primaryStage.setScene(scene);
		primaryStage.show();

		canvas = new Canvas(width, height);
		graphicsContext = canvas.getGraphicsContext2D();
		root.getChildren().add(canvas);
		drawStuff();
		lineArt();
	}

	public static void main(String[] args) {
		launch(args);
	}

	public void drawStuff() {
		// draw a circle
		graphicsContext.setStroke(Color.MIDNIGHTBLUE);
		graphicsContext.setLineWidth(10);
		graphicsContext.strokeOval(200, 200, 250, 250);

		// draw a rectangle
		graphicsContext.setStroke(Color.AQUAMARINE);
		graphicsContext.setLineWidth(5);
		graphicsContext.strokeRect(20, 50, 150, 350);

		// draw an arc
		graphicsContext.setStroke(Color.RED);
		graphicsContext.setLineWidth(3);
		graphicsContext.strokeArc(75, 75, 130, 40, 25, 90, ArcType.OPEN);

	}

	public void lineArt() {
		graphicsContext.setStroke(Color.BLUEVIOLET);
		graphicsContext.setLineWidth(2);

		int x1 = 10;
		int y1 = 15;
		int x2 = 500;
		int y2 = 15;
		int step = 25;
		for (int i = 0; i < 100; ++i) {
			graphicsContext.strokeLine(x1, y1, x2, y2);
			x1 += step;
			y2 += step;
		}
	}
}
